<?php

use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Kandjeng Nashir',
            'email' => 'nashir@butternutwebsite.com',
            'password' => bcrypt('blackdog'),
            'email_verified_at' => Carbon\Carbon::now(),
            'created_at' => Carbon\Carbon::now()
        ]);
        DB::table('users')->insert([
            'name' => 'Praditya Bagus',
            'email' => 'adit@butternutwebsite.com',
            'password' => bcrypt('pokokeyes'),
            'email_verified_at' => Carbon\Carbon::now(),
            'created_at' => Carbon\Carbon::now()
        ]);
    }
}
