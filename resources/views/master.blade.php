<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


	<!-- CSS -->
	<link rel="stylesheet" href="{{asset('vendor/fontawesome4.7.0/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('vendor/jquery-custom-scrollbar-0.5.5/jquery-custom-scrollbar-0.5.5/jquery.custom-scrollbar.css')}}" type="text/css">
    @stack('prestyle')
	<link id="theme" rel="stylesheet" href="{{asset('css/style-red-header.css')}}" type="text/css">
    @stack('styles')

	<title>Starter Boothelp Laravel</title>
</head>

<body class="fixed-header sticky-footer {{isset($bodyclass)?$bodyclass:''}}">
	<div class="loader-logo">
		<img src="{{asset('img/logo.png')}}" alt="logo">
		<br>
		<div class="loader-ellipsis">
			<div></div>
			<div></div>
			<div></div>
			<div></div>
		</div>
    </div>
    
	@yield('contentbody')
	
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="{{asset('vendor/jquery-3.2.1.min.js')}}"></script>
	<script src="{{asset('vendor/popper.min.js')}}"></script>
	<script src="{{asset('vendor/bootstrap-4.1.1/bootstrap.min.js')}}"></script>
    <script src="{{asset('vendor/cookie/jquery.cookie.js')}}"></script>
    <script src="{{asset('vendor/jquery-custom-scrollbar-0.5.5/jquery-custom-scrollbar-0.5.5/jquery.custom-scrollbar.min.js')}}"></script>
    <script src="{{asset('vendor/cicular_progress/circle-progress.min.js')}}"></script>
    <script src="{{asset('vendor/sparklines/jquery.sparkline.min.js')}}"></script>

    @stack('scripts')
    
    <script src="{{asset('js/main.js')}}"></script>
    
    @stack('scripts2')
</body>

</html>