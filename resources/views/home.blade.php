@extends('master',['bodyclass'=>'menuleft-open'])

@push('prestyle')
@endpush

@push('styles')
@endpush

@section('contentbody')
    <div class="wrapper">
        @include('includes.header')
        @include('includes.sidebar')
	    <div class="main-container">
            <div class="container-fluid mb-3 position-relative bg-redish">
                <div class="row">
                    <div class="container py-2">
                        <div class="row page-title-row">
                            <div class="col-8 col-md-6">
                                <h2 class="page-title text-white">Dashboard</h2>
                                <p class="text-white">Bootstrap Admin Dashboard Template with wide range components</p>
                            </div>
                            <div class="col col-md-6 text-right align-self-center">
                                <a href="#" class="btn btn-success" data-toggle="tooltip" title="Buy Now!" >
                                    <i class="fa fa-star"></i>
                                </a>
                                <button type="button" class="btn btn-primary ml-2" data-toggle="tooltip" title="Setting" id="color-setting">
                                    <i class="fa fa-cogs"></i>
                                </button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- Begin page content -->
            <div class="container">
                <div class="row">

                </div>
            </div>
        </div>
        @include('includes.sidebar_right')
        @include('includes.footer')
    </div>
@endsection

@push('scripts')
@endpush

@push('scripts2')
@endpush