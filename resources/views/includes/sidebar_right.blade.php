<div class="sidebar border-left right scroll-y">
    <div class="title">
        <i class="fa fa-subway text-success"></i> Highlights & Customize</div>
    <ul class="nav nav-tabs nav-justified" role="tablist">
        <li class="nav-item">
            <a class="nav-link active  border-success" id="tab1-tab" data-toggle="tab" href="#rightside" role="tab" aria-controls="rightside"
             aria-selected="true">
                <i class="fa fa-rocket"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link border-success" id="tab2-tab" data-toggle="tab" href="#chatlist" role="tab" aria-controls="chatlist" aria-selected="false">
                <i class="fa fa-comments"></i>
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade show active" id="rightside" role="tabpanel">
            <div class="userlist_large ">
                <div class="progress_profile " data-value="0.65" data-size="140" data-thickness="4" data-animation-start-value="0" data-reverse="false"></div>
                <div class="media">
                    <figure class="avatar120 rounded-circle m-auto">
                        <img src="{{\Auth::user()->getAvatar(256)}}" alt="user image">
                    </figure>

                    <div class="media-body">
                        <h4 class="my-3">65%
                            <span class="text-secondary">Completed</span>
                        </h4>
                        <p>Fill up your requiredinformations and complete your profile to avail offers</p>
                        <button type="button" class="btn btn-primary">Complete your profile
                            <i class="fa fa-angle-right"></i>
                        </button>
                    </div>
                </div>
            </div>
            <br>
            <hr>
            <h4 class="capstitle text-uppercase">New Friend Request</h4>
            <div class="list-unstyled userlist">
                <div class="item">
                    <a class="media" href="#">
                        <figure class="avatar30 rounded-circle z2 align-self-start">
                            <img src="../assets/img/user2.png" alt="Generic placeholder image">
                        </figure>
                        <div class="media-body">
                            <h5 class="time-title">John Smith
                                <small class="float-right">1 min ago</small>
                            </h5>
                            New Jersey, UK
                        </div>
                    </a>
                    <div class="button-wrap">
                        <button type="button" class="btn btn-danger sq-btn">
                            <i class="fa fa-close"></i>
                        </button>
                        <button type="button" class="btn btn-success sq-btn">
                            <i class="fa fa-check"></i>
                        </button>
                    </div>
                </div>
                <div class="item">
                    <a class="media" href="#">
                        <figure class="avatar30 rounded-circle z2 align-self-start">
                            <img src="../assets/img/user3.png" alt="Generic placeholder image">
                        </figure>
                        <div class="media-body">
                            <h5 class="time-title">Astha Anwar
                                <small class="float-right">
                                    <i class="circle-dot bg-success"></i> Online</small>
                            </h5>
                            New Jersey, UK
                        </div>
                    </a>
                    <div class="button-wrap">
                        <button type="button" class="btn btn-danger sq-btn">
                            <i class="fa fa-close"></i>
                        </button>
                        <button type="button" class="btn btn-success sq-btn">
                            <i class="fa fa-check"></i>
                        </button>
                    </div>
                </div>
                <div class="item">
                    <a class="media" href="#">
                        <figure class="avatar30 rounded-circle z2 align-self-start">
                            <img src="../assets/img/user4.png" alt="Generic placeholder image">
                        </figure>
                        <div class="media-body">
                            <h5 class="time-title">Tommy Boyoth
                                <small class="float-right">
                                    <i class="circle-dot bg-warning"></i> Away</small>
                            </h5>
                            New Jersey, UK
                        </div>
                    </a>
                    <div class="button-wrap">
                        <button type="button" class="btn btn-danger sq-btn">
                            <i class="fa fa-close"></i>
                        </button>
                        <button type="button" class="btn btn-success sq-btn">
                            <i class="fa fa-check"></i>
                        </button>
                    </div>
                </div>
            </div>
            <br>
            <hr>
            <h4 class="capstitle text-uppercase">New Event Request</h4>
            <div class="list-unstyled userlist">
                <div class="item">
                    <a class="media" href="#">
                        <i class="align-self-start icon fa fa-calendar"></i>
                        <div class="media-body">
                            <h5>20 February, 2017</h5>
                            <p>New Jersey, UK</p>
                            Musical night festival seasons, Drama and comedy cultural family show.
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a class="media" href="#">
                        <i class="align-self-start icon fa fa-calendar"></i>
                        <div class="media-body">
                            <h5>20 February, 2017</h5>
                            <p>New Jersey, UK</p>
                            Musical night festival seasons, Drama and comedy cultural family show.
                        </div>
                    </a>
                </div>
            </div>
            <br>
            <hr>
            <h4 class="capstitle text-uppercase">Last Message</h4>
            <div class="list-unstyled userlist">
                <div class="item">
                    <div class="media">
                        <figure class="avatar30 rounded-circle z2 align-self-start">
                            <img src="../assets/img/user4.png" alt="Generic placeholder image">
                        </figure>
                        <div class="media-body">
                            <h5 class="time-title">Tommy Boyoth
                                <small>
                                    <i class="circle-dot bg-warning"></i> Away</small>
                            </h5>
                            <p>New Jersey, UK</p>
                            Hi! Are you ready for Musical night festival seasons, Drama and comedy cultural family show.
                            <br>
                            <br>
                            <button class="btn btn-sm btn-primary">
                                <i class="fa fa-reply"></i> Reply</button>
                            <button class="btn btn-sm btn-outline-danger">Dismiss</button>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <hr>
            <h4 class="capstitle text-uppercase">Advertizing Block</h4>
            <figure class="w-100 text-center m-0 col">
                <img src="../assets/img/maxartkiller_website.png" class="mw-100" alt="maxartkiller, max art killer, web and ui designs">
                <figcaption>
                    <a class="text-center" href="www.maxartkiller.in" >www.maxartkiller.in</a>
                </figcaption>
            </figure>
        </div>
        <div class="tab-pane fade" id="chatlist" role="tabpanel">
            <div class="list-unstyled userlist">
                <a class="item" href="#">
                    <div class="media">
                        <figure class="avatar36 rounded-circle z2 align-self-start mr-3">
                            <img src="../assets/img/user2.png" alt="Generic placeholder image">
                        </figure>
                        <div class="media-body">
                            <h5 class="time-title">John Doe
                                <small class="float-right">1 min ago</small>
                            </h5>
                            Sent a message
                        </div>
                    </div>
                </a>
                <a class="item" href="#">
                    <div class="media">
                        <figure class="avatar36 rounded-circle z2 align-self-start mr-3">
                            <img src="../assets/img/user3.png" alt="Generic placeholder image">
                        </figure>
                        <div class="media-body">
                            <h5 class="time-title">
                                <span class="circle-dot bg-warning"></span>Astha Anwar
                                <small class="float-right">1 min ago</small>
                            </h5>
                            Sent a message
                        </div>
                    </div>
                </a>
                <a class="item" href="#">
                    <div class="media">
                        <figure class="avatar36 rounded-circle z2 align-self-start mr-3">
                            <img src="../assets/img/user1.png" alt="Generic placeholder image">
                        </figure>
                        <div class="media-body">
                            <h5 class="time-title">
                                <span class="circle-dot bg-danger"></span>Maxartkiller
                                <small class="float-right">1 min ago</small>
                            </h5>
                            Sent a message
                        </div>
                    </div>
                </a>
                <a class="item" href="#">
                    <div class="media">
                        <figure class="avatar36 rounded-circle z2 align-self-start mr-3">
                            <img src="../assets/img/user4.png" alt="Generic placeholder image">
                        </figure>
                        <div class="media-body">
                            <h5 class="time-title">
                                <span class="circle-dot bg-success"></span>Tommy Bayoth
                                <small class="float-right">1 hour ago</small>
                            </h5>
                            Sent a message
                        </div>
                    </div>
                </a>
                <a class="item" href="#">
                    <div class="media">
                        <figure class="avatar36 rounded-circle z2 align-self-start mr-3">
                            <img src="../assets/img/user1.png" alt="Generic placeholder image">
                        </figure>
                        <div class="media-body">
                            <h5 class="time-title">
                                <span class="circle-dot bg-danger"></span>Maxartkiller
                                <small class="float-right">1 min ago</small>
                            </h5>
                            Sent a message
                        </div>
                    </div>
                </a>
                <a class="item" href="#">
                    <div class="media">
                        <figure class="avatar36 rounded-circle z2 align-self-start mr-3">
                            <img src="../assets/img/user4.png" alt="Generic placeholder image">
                        </figure>
                        <div class="media-body">
                            <h5 class="time-title">
                                <span class="circle-dot bg-success"></span>Tommy Bayoth
                                <small class="float-right">1 hour ago</small>
                            </h5>
                            Sent a message
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>


    <br>
    <br>
</div>
