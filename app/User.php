<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * generate avatar from user initial
     * 
     * @param int $size default 64, big=128, large=512
     * @return string url to ui-avatar
     */
    public function getAvatar( $size = 64 ) {
        $name= urlencode($this->name);
        return "https://ui-avatars.com/api/?size=$size&name=$name&background=fff&color=7b1c16";
      }
}
