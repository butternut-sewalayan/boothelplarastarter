## HOW TO
1. git clone this
2. run composer install
3. run 'cp .env.example .env'
4. edit .env terutama di bagian mysql sesuaikan dengan database local
5. run 'php artisan key:generate'
6. migrate awal, run 'php artisan migrate'
7. seed database, run 'php artisan db:seed'